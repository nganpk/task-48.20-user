$(document).ready(function(){
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //Biến chứa dữ liệu lấy từ api
    var gUserDB = {
        users:[],
        //phương thức lọc country
        filterCountry: function(paramFilterObj){
            var vFilterResult = [];
            vFilterResult = this.users.filter((paramCountry) => {
                return (paramCountry.country.toUpperCase() == paramFilterObj.country  ||  paramFilterObj.country == "ALL"); 
            })
            return vFilterResult;
        },//phương thức thay thế khi sửa dữ liệu
        changeObj: function (paramResponseObj){
            this.users.map((paramUser,paramIndex)=>{
              if(paramUser.id === paramResponseObj.id){
                return  this.users.splice(paramIndex, 1, paramResponseObj);
              }
            })
        }
        
    }

    //Biến chứa stt
    var gSTT = 1;

    var gId = [];
    //Tạo biến chứa thuộc tính của bảng
    const gUSER_COLUMNS = ["stt", "firstname", "lastname", "country", "subject", "customerType", "registerStatus", "action"]
    //thứ tự các cột
    const gCOL_STT = 0;
    const gCOL_FIRST_NAME = 1;
    const gCOL_LAST_NAME = 2;
    const gCOL_COUNTRY = 3;
    const gCOL_SUBJECT = 4;
    const gCOL_CUSTOMER_TYPE = 5;
    const gCOL_REGISTER_STATUS = 6;
    const gCOL_ACTION = 7;

    //TẠO Bảng
    var gUserTable = $('#user-table').DataTable({
        columns:[
            {data: gUSER_COLUMNS[gCOL_STT]},
            {data: gUSER_COLUMNS[gCOL_FIRST_NAME]},
            {data: gUSER_COLUMNS[gCOL_LAST_NAME]},
            {data: gUSER_COLUMNS[gCOL_COUNTRY]},
            {data: gUSER_COLUMNS[gCOL_SUBJECT]},
            {data: gUSER_COLUMNS[gCOL_CUSTOMER_TYPE]},
            {data: gUSER_COLUMNS[gCOL_REGISTER_STATUS]},
            {data: gUSER_COLUMNS[gCOL_ACTION]}
        ],
        columnDefs:[
            {//định nghĩa lại cột STT
                targets: gCOL_STT,
                render: function(){
                    return gSTT ++
                }

            },
            {//ĐỊNH NGHĨA LẠI CỘT ACTION
                targets: gCOL_ACTION,
                defaultContent: `
                    <button class="btn btn-success user-detail">Chi tiết</button>
                `
            }
        ]
    });
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện cho nút lọc dữ liệu
    $('#btn-filter-data').on('click', onBtnFilterCountryClick);
    //gán sự kiện cho nút chi tiết
    $('#user-table').on('click', '.user-detail', function(){
        onBtnUserDetailClick(this);
    })
    
    //reset dữ liệu khi click nút cancel
    $('#user-modal').on('hidden.bs.modal', function () {
        resetForm();
    })
    //gán sự kiện cho nút save data
    $('#btn-save-data').on('click', onBtnSaveDataClick)
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm xử lý sự kiện load trang
    function onPageLoading(){
      "use strict";
      //Load dữ liệu khách hàng vào bảng
      callApiToLoadDataToTable();
    }

    //Hàm xử lý sự kiện ấn nút lọc dữ liệu country
    function onBtnFilterCountryClick(){
        "use strict";
        console.log('hello')
        //tạo đối tượng
        var vFilterObj = {
            country:""
        }
        var vFilterResult = [];
        //Thu thập dữ liệu
        vFilterObj.country = $('#select-country').val();
        if(vFilterObj.country != ""){
            //Lọc dữ liệu
            vFilterResult = gUserDB.filterCountry(vFilterObj);
            console.log(vFilterResult);
        
            //Hiển thị dữ liệu lên bảng
            loadDataToTable(vFilterResult);
        }
        //Lọc dữ liệu các nước khác
        if(vFilterObj.country === ""){
            vFilterResult = gUserDB.users.filter((value)=>{
                
                return (
                    (value.country.toUpperCase() != "VN")&&
                    (value.country.toUpperCase() != "USA")&&
                    (value.country.toUpperCase() != "CAN")&&
                    (value.country.toUpperCase() != "AUS")
                )
            })
            console.log(vFilterResult)
            loadDataToTable(vFilterResult);
        }
        
    }
    //Hàm xử lý sự kiện ấn nút chi tiết
    function onBtnUserDetailClick(paramDetailButton){
        "use strict";
        var vRowSelected = $(paramDetailButton).parents('tr')
        var vRow = gUserTable.row(vRowSelected);
        var vRowData = vRow.data();

        //lấy id
        gId = vRowData.id;
        console.log(gId);

        var vCountry = vRowData.country
        console.log(vCountry);
        //Hiện modal
        $('#user-modal').modal('show')

        //Hiển thị dữ liệu lên modal
        showDataToModal(vRowData);
        
    }

    //Hàm xử lý sự kiện ấn nút save data
    function onBtnSaveDataClick(){
        "use strict";
        //tạo đối tượng
        var vUserObj = {
            firstname:"",
            lastname:"",
            subject:"",
            country:"",
            customerType:"",
            registerStatus:""
        }
        //Thu thập dữ liệu
        getUserData(vUserObj);

        var vIsValidated = validateData(vUserObj);
        if(vIsValidated){
            callApiToUpdateData(vUserObj)
            //cập nhật dữ liệu thành công
            alert('Cập nhật dữ liệu thành công');
            //tắt modal
            $('#user-modal').modal('hide')
            //reset modal
            resetForm();
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm xử lý sự kiện gọi Api load dữ liệu vào bảng
    function callApiToLoadDataToTable(){
      "use strict";
      $.ajax({
          type: "GET",
          url: "http://42.115.221.44:8080/devcamp-register-java-api/users",
          dataType: "json",
          success: function(paramResObj){
              gUserDB.users = paramResObj;
              loadDataToTable(gUserDB.users);
          },
          error: function(paramErr){
              console.log(paramErr);
          }
      })
    }
    
    //Hàm xử lý load dữ liệu vào bảng
    function loadDataToTable(paramUserArr){
        "use strict";
        gSTT = 1;
        gUserTable.clear();
        gUserTable.rows.add(paramUserArr);
        gUserTable.draw(false);
    }

    //Hàm show dữ liệu lên modal
    function showDataToModal(paramData){
        "use strict";
        $('#inp-fname').val(paramData.firstname);
        $('#inp-lname').val(paramData.lastname);
        var vSelectCountry = $('#select-new-country').val();
        if(paramData.country === vSelectCountry){
            
            $('#select-new-country').val(paramData.country)
        } else {//trường hợp là nước khác ngoài 4 nước chính
            var vOptionalOption = $('#optional-option');
            vOptionalOption.text(paramData.country);
            vOptionalOption.val(paramData.country);
            var vOptionValue = vOptionalOption.val();
            $('#select-new-country').val(vOptionValue);
        }

        
       
        
        $('#inp-subject').val(paramData.subject);
        $('#inp-customer').val(paramData.customerType);
        //trường hợp register status
        var vSelectRegister = $('#select-register-status').val();
        if(paramData.registerStatus === vSelectRegister){
            $('#select-register-status').val(paramData.registerStatus)
            
        } else {//trường hợp là nước khác ngoài 4 nước chính
            var vOptionalRegisterOption = $('#optional-register-option');
            // vOptionalRegisterOption.show();
            vOptionalRegisterOption.text(paramData.registerStatus);
            vOptionalRegisterOption.val(paramData.registerStatus);
            var vOptionRegisterValue = vOptionalRegisterOption.val();
            $('#select-register-status').val(vOptionRegisterValue);
        }

        
    }

    //Hàm xử lý thu thập dữ liệu
    function getUserData(paramUserData){
        "use strict";
        paramUserData.firstname = $('#inp-fname').val().trim();
        paramUserData.lastname = $('#inp-lname').val().trim();
        paramUserData.country = $('#select-new-country').val();
        paramUserData.subject = $('#inp-subject').val().trim();
        paramUserData.customerType = $('#inp-customer').val().trim();
        paramUserData.registerStatus = $('#select-register-status').val();
    }

    //Hàm validate dữ liệu
    function validateData(paramUserData){
        "use strict";
        var vModalWarning = $('#modal-warning');
        var vModalWarningBody = $('#modal-body');
        //validate firstname
        if(paramUserData.firstname === ""){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập firstname')
            return false;
        }
        //validate lastname
        if(paramUserData.lastname === ""){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập lastname')
            return false;
        }
        //validate subject
        if(paramUserData.subject === ""){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập subject')
            return false;
        }
        //validate customer Type
        if(paramUserData.customerType === ""){
            vModalWarning.modal('show');
            vModalWarningBody.html('Xin vui lòng nhập customer type')
            return false;
        }
       
        return true;
    }
    
    //Hàm gọi api update dữ liệu
    function callApiToUpdateData(paramObj){
        "use strict";
        $.ajax({
            type: "PUT",
            url: "http://42.115.221.44:8080/devcamp-register-java-api/users/" + gId,
            contentType:"application/json;charset=UTF-8",
            data: JSON.stringify(paramObj), 
            success: function(paramResObj){
               console.log(paramResObj)
               
               gUserDB.changeObj(paramResObj);
             
               loadDataToTable(gUserDB.users)
            },
            error: function(paramErr){
                console.log(paramErr);
            }
        })
    }

    //Hàm reset các trường dữ liệu
    function resetForm(){
        "use strict";
        $('#inp-fname').val("");
        $('#inp-lname').val("");
        $('#select-new-country').val("")
        $('#inp-subject').val("");
        $('#inp-customer').val("");
        $('#select-register-status').val("");
    }

})